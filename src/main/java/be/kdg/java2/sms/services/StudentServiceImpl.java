package be.kdg.java2.sms.services;

import be.kdg.java2.sms.domain.Student;
import be.kdg.java2.sms.repository.HardcodedStudentRepository;
import be.kdg.java2.sms.repository.StudentRepository;

import java.time.LocalDate;
import java.util.List;

public class StudentServiceImpl implements StudentService {
    private StudentRepository repository;

    public StudentServiceImpl(StudentRepository repository) {
        this.repository = repository;
    }

    @Override
    public Student addStudent(String name, LocalDate birthDay, double length){
        Student student = new Student(name, birthDay, length);
        return repository.createStudent(student);
    }

    @Override
    public List<Student> getAllStudents(){
        return repository.readStudents();
    }
}
