package be.kdg.java2.sms.services;

import be.kdg.java2.sms.domain.Student;

import java.time.LocalDate;
import java.util.List;

public interface StudentService {
    Student addStudent(String name, LocalDate birthDay, double length);
    List<Student> getAllStudents();
}
