package be.kdg.java2.sms.services;

import be.kdg.java2.sms.domain.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DummyStudentService implements StudentService{
    @Override
    public Student addStudent(String name, LocalDate birthDay, double length) {
        System.out.println("Dummy: adding student...");
        return new Student("dummy", LocalDate.now(),1);
    }

    @Override
    public List<Student> getAllStudents() {
        System.out.println("Dummy: get all students...");
        return new ArrayList<>();
    }
}
