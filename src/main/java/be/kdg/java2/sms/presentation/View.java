package be.kdg.java2.sms.presentation;

import be.kdg.java2.sms.services.DummyStudentService;
import be.kdg.java2.sms.services.StudentService;
import be.kdg.java2.sms.services.StudentServiceImpl;

import java.time.LocalDate;
import java.util.Scanner;

public class View {
    private StudentService studentService;
    private Scanner scanner = new Scanner(System.in);
    private boolean quit = false;

    public View(StudentService studentService) {
        this.studentService = studentService;
    }

    public void show(){
        while (!quit) {
            System.out.println("Student Management System");
            System.out.println("=========================");
            System.out.println("0) exit");
            System.out.println("1) show all students");
            System.out.println("2) add a student");
            int choice = scanner.nextInt();
            respondToAnswer(choice);
        }
    }

    private void respondToAnswer(int choice) {
        switch (choice) {
            case 0: quit = true;return;
            case 1: showAllStudents();break;
            case 2: addStudent();break;
        }
    }

    private void showAllStudents() {
        studentService.getAllStudents().forEach(System.out::println);
    }

    private void addStudent(){
        //ask for name and stuff
        studentService.addStudent("dummy", LocalDate.of(1974, 4, 2), 1.78);
    }
}
