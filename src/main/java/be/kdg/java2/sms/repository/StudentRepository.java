package be.kdg.java2.sms.repository;

import be.kdg.java2.sms.domain.Student;

import java.util.List;

public interface StudentRepository {
    Student createStudent(Student student);
    List<Student> readStudents();
}
