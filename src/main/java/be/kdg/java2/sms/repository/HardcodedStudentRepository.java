package be.kdg.java2.sms.repository;

import be.kdg.java2.sms.domain.Student;

import java.util.ArrayList;
import java.util.List;

public class HardcodedStudentRepository implements StudentRepository {
    private static List<Student> students = new ArrayList<>();
    @Override
    public Student createStudent(Student student){
        student.setId(students.size() + 1);
        students.add(student);
        return student;
    }

    @Override
    public List<Student> readStudents(){
        return students;
    }
}
