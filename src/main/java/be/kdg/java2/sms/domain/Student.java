package be.kdg.java2.sms.domain;

import java.time.LocalDate;

public class Student {
    private int id;
    private String name;
    private LocalDate birthDay;
    private double length;

    public Student(String name, LocalDate birthDay, double length) {
        this.name = name;
        this.birthDay = birthDay;
        this.length = length;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public double getLength() {
        return length;
    }
}
