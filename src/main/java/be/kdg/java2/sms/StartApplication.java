package be.kdg.java2.sms;

import be.kdg.java2.sms.presentation.View;
import be.kdg.java2.sms.repository.HardcodedStudentRepository;
import be.kdg.java2.sms.repository.StudentRepository;
import be.kdg.java2.sms.services.StudentService;
import be.kdg.java2.sms.services.StudentServiceImpl;

public class StartApplication {
    public static void main(String[] args) {
        StudentRepository studentRepository = new HardcodedStudentRepository();
        StudentService studentService = new StudentServiceImpl(studentRepository);
        View view = new View(studentService);
        view.show();
    }
}
